// ==UserScript==
// @name           Pardus Forum Blocker
// @namespace      http://userscripts.xcom-alliance.info/
// @description    Blocks access to the Pardus forums and links to threads in the forums
// @author         Miche (Orion) / Sparkle (Artemis)
// @include        http*://*.pardus.at/menu.php
// @include        http*://forum.pardus.at/*
// @version        1.3
// @updateURL      http://userscripts.xcom-alliance.info/forum_blocker/pardus_forum_blocker.meta.js
// @downloadURL    http://userscripts.xcom-alliance.info/forum_blocker/pardus_forum_blocker.user.js
// @icon           http://userscripts.xcom-alliance.info/forum_blocker/icon.png
// @grant          none
// ==/UserScript==
