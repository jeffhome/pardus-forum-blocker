// ==UserScript==
// @name           Pardus Forum Blocker
// @namespace      http://userscripts.xcom-alliance.info/
// @description    Blocks access to the Pardus forums and links to threads in the forums
// @author         Miche (Orion) / Sparkle (Artemis)
// @include        http*://*.pardus.at/menu.php
// @include        http*://forum.pardus.at/*
// @version        1.3
// @updateURL      http://userscripts.xcom-alliance.info/forum_blocker/pardus_forum_blocker.meta.js
// @downloadURL    http://userscripts.xcom-alliance.info/forum_blocker/pardus_forum_blocker.user.js
// @icon           http://userscripts.xcom-alliance.info/forum_blocker/icon.png
// @grant          none
// ==/UserScript==

/*****************************************************************************************
	Version Information

	1.3 ( 06-May-2013 )
	- initial release for general use

*****************************************************************************************/

/*****************************************************************************************
   Make the Forum button from the top menu disappear
*****************************************************************************************/

if (document.URL.search('pardus.at/menu.php') > -1) {

	var aEls = document.getElementsByTagName('a');
	for (var loop=0; loop<aEls.length; loop++) {
		if (aEls[loop].href.indexOf('://forum.pardus.at')>-1) {
			aEls[loop].parentNode.style.display = 'none';
			aEls[loop].parentNode.nextElementSibling.style.display = 'none';
		}
	}

}

/*****************************************************************************************
   If you follow any link to the Forums, this will send you right back again
*****************************************************************************************/

if (document.URL.search('forum.pardus.at/') > -1) {

	if (document.referrer) history.go(-1);

}

